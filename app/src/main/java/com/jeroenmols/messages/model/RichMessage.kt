package com.jeroenmols.messages.model

import androidx.room.Relation

internal data class RichMessage(
    val id: Int,
    val userId: Int,
    val content: String,
    val userAvatarId: String,
    @Relation(parentColumn = "id", entityColumn = "messageId")
    val attachments: List<Attachment>
)