package com.jeroenmols.messages.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
internal data class User(
    @PrimaryKey val id: Long,
    val name: String,
    val avatarId: String
)