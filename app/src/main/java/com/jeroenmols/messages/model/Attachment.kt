package com.jeroenmols.messages.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
internal data class Attachment(
    @PrimaryKey val id: String,
    val title: String,
    val url: String,
    val thumbnailUrl: String,
    val messageId: Int = -1
)