package com.jeroenmols.messages.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
internal data class Message @JvmOverloads constructor(
    @PrimaryKey val id: Int,
    val userId: Int,
    val content: String,
    @Ignore val attachments: List<Attachment> = emptyList(),
    @Ignore val userAvatarId : String = ""
)