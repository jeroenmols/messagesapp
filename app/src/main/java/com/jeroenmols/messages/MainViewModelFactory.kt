package com.jeroenmols.messages

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.jeroenmols.messages.database.MessageDatabase

internal class MainViewModelFactory(private val applicationContext: Context) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            val db: MessageDatabase = Room.databaseBuilder(
                applicationContext,
                MessageDatabase::class.java,
                MessageDatabase.DATABASE_NAME
            ).build()
            return MainViewModel(db) as T
        }
        throw RuntimeException("Can't create ViewModel - Unsupported Viewmodel class")
    }

}