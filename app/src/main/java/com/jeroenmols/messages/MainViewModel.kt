package com.jeroenmols.messages;

import android.content.Context
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.jeroenmols.messages.MessageItem.FromMe
import com.jeroenmols.messages.MessageItem.FromOther
import com.jeroenmols.messages.database.DataImporter
import com.jeroenmols.messages.database.MessageDatabase
import com.jeroenmols.messages.model.RichMessage
import kotlinx.coroutines.launch

internal class MainViewModel(private val database: MessageDatabase) : ViewModel() {

    val messages = MediatorLiveData<PagedList<MessageItem<*>>>()

    fun doOnStart(applicationContext: Context) {
        viewModelScope.launch {
            DataImporter().runOnlyOnce(applicationContext, database)
            val messageSource = database.messageDao().getAllPaged().map(::toMessageItem)
            messages.addSource(messageSource.toLiveData(PAGE_SIZE)) { messages.value = it }
        }
    }

    private fun toMessageItem(message: RichMessage): MessageItem<*> =
        if (message.userId == USER_ME) FromMe(message) else FromOther(message)

    fun delete(message: MessageItem<*>) {
        viewModelScope.launch { database.messageDao().delete((message.data as RichMessage).id) }
    }

    companion object {
        private const val USER_ME = 1
        private const val PAGE_SIZE = 20
    }
}

