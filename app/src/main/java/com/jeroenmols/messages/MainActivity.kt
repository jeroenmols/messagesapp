package com.jeroenmols.messages

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeroenmols.messages.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: MessageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = MessageAdapter(::showDeleteDialog)
        binding.recyclerviewMessages.adapter = adapter
        binding.recyclerviewMessages.layoutManager = LinearLayoutManager(this)

        viewModel = ViewModelProviders.of(this, MainViewModelFactory(applicationContext)).get(MainViewModel::class.java)
        viewModel.messages.observe(this, Observer { adapter.submitList(it) })
    }

    private fun showDeleteDialog(message: MessageItem<*>) {
        AlertDialog.Builder(this).setMessage(getString(R.string.delete_dialog_title))
            .setPositiveButton(getString(R.string.delete_dialog_delete)) { _, _ -> viewModel.delete(message) }
            .setNegativeButton(getString(R.string.delete_dialog_keep)) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    override fun onStart() {
        super.onStart()
        viewModel.doOnStart(this.applicationContext)
    }
}