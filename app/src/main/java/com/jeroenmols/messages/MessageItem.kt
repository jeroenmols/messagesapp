package com.jeroenmols.messages

import com.jeroenmols.messages.model.RichMessage

internal sealed class MessageItem<T> {
    abstract val data: T

    data class FromMe(override val data: RichMessage) : MessageItem<RichMessage>()
    data class FromOther(override val data: RichMessage) : MessageItem<RichMessage>()
}