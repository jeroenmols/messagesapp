package com.jeroenmols.messages

import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.CircleCropTransformation
import com.jeroenmols.messages.MessageAdapter.MessageViewHolder
import com.jeroenmols.messages.MessageAdapter.MessageViewHolder.FromMe
import com.jeroenmols.messages.MessageAdapter.MessageViewHolder.FromOther
import com.jeroenmols.messages.databinding.ItemMessageMeBinding
import com.jeroenmols.messages.databinding.ItemMessageOtherBinding

internal typealias ItemClickListener = (MessageItem<*>) -> Unit

internal class MessageAdapter(private val listener: (MessageItem<*>) -> Unit) :
    PagedListAdapter<MessageItem<*>, MessageViewHolder>(MessageDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return when (viewType) {
            1 -> FromMe(ItemMessageMeBinding.inflate(from(parent.context), parent, false), listener)
            2 -> FromOther(ItemMessageOtherBinding.inflate(from(parent.context), parent, false), listener)
            else -> throw RuntimeException("Unsupported view type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is MessageItem.FromMe -> 1
            is MessageItem.FromOther -> 2
            null -> TODO("show placeholder here")
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        when (holder) {
            is FromMe -> holder.bind(getItem(position) as MessageItem.FromMe)
            is FromOther -> holder.bind(getItem(position) as MessageItem.FromOther)
        }
    }

    private object MessageDiffCallback : DiffUtil.ItemCallback<MessageItem<*>>() {
        override fun areItemsTheSame(oldItem: MessageItem<*>, newItem: MessageItem<*>): Boolean {
            return if (oldItem is MessageItem.FromMe && newItem is MessageItem.FromMe) {
                oldItem.data.id == newItem.data.id
            } else if (oldItem is MessageItem.FromOther && newItem is MessageItem.FromOther) {
                oldItem.data.id == newItem.data.id
            } else {
                false
            }
        }

        override fun areContentsTheSame(oldItem: MessageItem<*>, newItem: MessageItem<*>): Boolean {
            return true // simple solution, data never changes in database
        }
    }

    internal abstract class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        abstract val clickListener: ItemClickListener

        class FromMe(private val binding: ItemMessageMeBinding,
                     override val clickListener: ItemClickListener) : MessageViewHolder(binding.root) {
            fun bind(message: MessageItem.FromMe) {
                binding.content.text = message.data.content
                binding.root.setOnClickListener { clickListener.invoke(message) }
            }
        }

        class FromOther(private val binding: ItemMessageOtherBinding,
                        override val clickListener: ItemClickListener) : MessageViewHolder(binding.root) {
            fun bind(message: MessageItem.FromOther) {
                binding.content.text = message.data.content
                binding.avatar.load(message.data.userAvatarId) { transformations(CircleCropTransformation()) }
                binding.root.setOnClickListener { clickListener.invoke(message) }
            }
        }
    }
}