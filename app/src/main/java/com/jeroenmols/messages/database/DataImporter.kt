package com.jeroenmols.messages.database

import android.content.Context
import android.content.res.Resources
import com.jeroenmols.messages.R
import com.jeroenmols.messages.model.Message
import com.jeroenmols.messages.model.User
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okio.buffer
import okio.source

internal class DataImporter {

    suspend fun runOnlyOnce(appContext: Context, database: MessageDatabase) {
        if (!database.isDataImported()) {
            val data = loadDataFromJson(appContext.resources)
            if (data == null) {
                throw RuntimeException("Failed to import data")
            } else {
                importDataInDatabase(database, data)
            }
        }
    }

    private suspend fun MessageDatabase.isDataImported() = messageDao().getAll().count() > 0

    private suspend fun loadDataFromJson(resources: Resources): RawJson? = withContext(Dispatchers.IO) {
        resources.openRawResource(R.raw.data).use {
            val buffer = it.source().buffer()
            val moshi = Moshi.Builder().build()
            val jsonAdapter = moshi.adapter(RawJson::class.java)
            jsonAdapter.fromJson(buffer)
        }
    }

    private suspend fun importDataInDatabase(database: MessageDatabase, data: RawJson) {
        database.userDao().insert(data.users)
        database.messageDao().insert(data.messages)
        database.attachmentDao().insert(data.messages.flatMap { message -> message.attachments?.map { it.copy( messageId = message.id) } ?: emptyList() })

        println("Imported ${database.messageDao().getAll().size} messages")
        println("Imported ${database.attachmentDao().getAll().size} attachments")
        println("Imported ${database.userDao().getAll().size} users")
    }

    private data class RawJson(val messages: List<Message>, val users: List<User>)
}