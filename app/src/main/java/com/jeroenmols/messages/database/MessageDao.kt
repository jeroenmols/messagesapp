package com.jeroenmols.messages.database

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.jeroenmols.messages.model.Message
import com.jeroenmols.messages.model.RichMessage

@Dao
internal interface MessageDao {
    @Insert
    suspend fun insert(messages: List<Message>)

    @Query("select * from message")
    suspend fun getAll(): List<Message>

    @Query("select message.id as id, message.content as content, message.userId as userId, user.avatarId as userAvatarId from message, user where message.userId = user.id")
    fun getAllPaged(): DataSource.Factory<Int, RichMessage>

    @Query("delete from message where id = :messageId")
    suspend fun delete(messageId: Int)
}