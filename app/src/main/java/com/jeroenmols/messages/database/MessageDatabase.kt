package com.jeroenmols.messages.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.jeroenmols.messages.database.MessageDatabase.Companion.DATABASE_VERSION
import com.jeroenmols.messages.model.Attachment
import com.jeroenmols.messages.model.Message
import com.jeroenmols.messages.model.User

@Database(entities = arrayOf(User::class, Message::class, Attachment::class), version = DATABASE_VERSION)
internal abstract class MessageDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun messageDao(): MessageDao
    abstract fun attachmentDao(): AttachmentDao

    companion object {
        const val DATABASE_NAME = "messages"
        const val DATABASE_VERSION = 1
    }
}