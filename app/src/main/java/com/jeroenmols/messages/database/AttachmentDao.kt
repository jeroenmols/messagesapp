package com.jeroenmols.messages.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.jeroenmols.messages.model.Attachment

@Dao
internal interface AttachmentDao {
    @Insert
    suspend fun insert(attachments: List<Attachment>)

    @Query("select * from attachment")
    suspend fun getAll(): List<Attachment>
}