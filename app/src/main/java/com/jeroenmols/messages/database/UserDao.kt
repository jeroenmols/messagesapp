package com.jeroenmols.messages.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.jeroenmols.messages.model.User

@Dao
internal interface UserDao {
    @Insert
    suspend fun insert(users: List<User>)

    @Query("select * from user")
    suspend fun getAll() : List<User>
}